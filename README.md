# Автоустановка стека ELK на Docker #
*Elasticsearch, Logstash, Kibana*

Форк от: https://github.com/spujadas/elk-docker

Готовый образ контейнера: https://hub.docker.com/r/rebirther/elk/

Что доработано:

* Оптимизировано для установки в Docker, который запущен на виртуальной машине (KVM, VMware)
* Конфигурация logstash из коробки может принимать syslog на 1514 порту в контейнере и на 514 порту снаружи, как для UDP так и для TCP
* Kibana при первом запуске контейнера получит Dashboard с несколькими полезными визуализациями
* Индекс для ElasticSearch будет принят автоматически

Рекомендации при установке:
* Желательно чтобы источник был готов отправлять syslog до установки и запуска контейнера

Playbook для Ansible, который развернёт этот образ на CentOS, RHEL: https://bitbucket.org/rebirther/ansible